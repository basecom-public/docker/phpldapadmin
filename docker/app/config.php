<?php
$servers = new Datastore();

$PHPLDAPADMIN_LDAP_NAME = getenv('PHPLDAPADMIN_LDAP_NAME');
$PHPLDAPADMIN_LDAP_HOST = getenv('PHPLDAPADMIN_LDAP_HOST');
$PHPLDAPADMIN_LDAP_PORT = getenv('PHPLDAPADMIN_LDAP_PORT');

$servers->newServer('ldap_pla');
$servers->setValue('server','name',$PHPLDAPADMIN_LDAP_NAME);
$servers->setValue('server','host',$PHPLDAPADMIN_LDAP_HOST);
$servers->setValue('server','port',$PHPLDAPADMIN_LDAP_PORT);

$config->custom->appearance['hide_template_warning'] = true;
?>
