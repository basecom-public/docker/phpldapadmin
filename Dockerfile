# https://endoflife.date/alpine
FROM webdevops/php-apache:7.3-alpine

RUN apk add --no-cache phpldapadmin \
  && rm -rf /app/ \
  && ln -s /usr/share/webapps/phpldapadmin/ /app \
  && chown -R 1000:1000 /app/ \
  && chown -R 1000:1000 /etc/phpldapadmin/

COPY docker/app/config.php /app/config/config.php

WORKDIR /app/
