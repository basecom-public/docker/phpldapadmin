# phpldapadmin

Dockerized [phpLDAPadmin](https://github.com/leenooks/phpLDAPadmin) with [webdevops/php-apache](https://dockerfile.readthedocs.io/en/latest/content/DockerImages/dockerfiles/php-apache.html) as base image

## Environment variables

See .env.app for environment variables
